<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionFacts extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nutrition_facts';
    protected $hidden = ['id', 'food_id', 'created_at', 'updated_at'];

    /**
     * Get the food that has this nutrition facts
     */
    public function food()
    {
        return $this->belongsTo('App\Food');
    }
}
