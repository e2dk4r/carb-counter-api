<?php

namespace App\Http\Controllers;

# imports the Google Cloud client library
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

use Illuminate\Http\Request;

class VisionController extends Controller
{
    public function get()
    {
        # instantiates a client
        $imageAnnotator = new ImageAnnotatorClient();

        # the name of the image file to annotate
        $fileName = storage_path('app/pics/last');

        # prepare the image to be annotated
        $image = file_get_contents($fileName);

        # performs label detection on the image file
        $response = $imageAnnotator->labelDetection($image);
        $labels = $response->getLabelAnnotations();

        if ($labels) {
            $response = [];
            // echo ("Labels:" . PHP_EOL);
            foreach ($labels as $label) {
               $response[] = $label->getDescription();
            }

            return response()->json($response, 200);
        } else {
            // echo ('No label found' . PHP_EOL);
            return response()->json(['msg' => 'No label found'], 404);
        }
    }
}
