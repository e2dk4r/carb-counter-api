<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    protected $hidden = ['id', 'food_id', 'created_at', 'updated_at'];
    
    /**
     * Get the food that has this barcode
     */
    public function food()
    { 
        return $this->belongsTo('App\Food');
    }
}
