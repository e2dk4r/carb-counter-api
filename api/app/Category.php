<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $hidden = ['food_id', 'created_at', 'updated_at'];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Get the food that has this category
     */
    public function foods()
    { 
        return $this->hasMany('App\Food');
    }
}
